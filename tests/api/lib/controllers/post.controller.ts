import { ApiRequest } from "../request";

export class PostController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("GET")
            .url(`api/Posts`)
            .bearerToken()
            .send();
        return response;
    }

    async createPost(authorIdValue: number, postIdValue: number, postBody: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId: authorIdValue,
                postId: postIdValue,
                body: postBody,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addReaction(postIdValue: number, islikeValue, authorIdValue: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                postId: postIdValue,
                isLike: islikeValue,
                authorId: authorIdValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addComment(authorIdValue: number, postIdValue: number, commentBody: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Comments`)
            .body({
                authorId: authorIdValue,
                postId: postIdValue,
                body: commentBody,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}