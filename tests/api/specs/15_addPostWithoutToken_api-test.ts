import { expect } from "chai";
import { PostController } from "../lib/controllers/post.controller";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";

const posts = new PostController();
const auth = new AuthController();

describe("Create the post without token", () => {
    let accessToken: string;
    let id;

    before(async () => {
        // This code will be executed before any test cases are run
        let response = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(response, 200);
        accessToken = ",";
        id = response.body.user.id;
    });

    it('Create post without token', async () => {
        let response = await posts.createPost(id, 0, "Art is never finished.", accessToken)
        console.log(response.body);
        checkStatusCode(response, 401);
        checkResponseTime(response, 500);
    });
});