import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new GetUserController();
const auth = new AuthController();

describe("Get user from token", () => {
    let accessToken: string;
    let userDataId, userDataToken;

    before(`Login and get the token`, async () => {
        let response = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(response, 200);
        accessToken = response.body.token.accessToken.token;
        userDataId = response.body;
    });

    it(`Get user from token`, async () => {
        let response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        userDataToken = response.body;
    });
});
