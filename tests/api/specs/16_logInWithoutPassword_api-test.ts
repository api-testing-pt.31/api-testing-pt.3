import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new GetUserController();
const auth = new AuthController();

describe("Log without password", () => {
    let accessToken: string;
    it(`Login without password`, async () => {
        let response = await auth.login("myNewEmail@gmail.com", " ");
  //      accessToken = response.body.token.accessToken.token;
   //     console.log(accessToken);
        expect(response.statusCode, `Status Code should be 401`).to.be.equal(401);
    });
});
