import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new GetUserController();
const auth = new AuthController();

describe("Get user from Id", () => {
    let id: number;
    let userDataLogin, userDataFromId;

    before(`Login and get the Id`, async () => {
        let response = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(response, 200);
        id = response.body.user.id;
        userDataLogin = response.body;
    });

    it(`Get user data from Id`, async () => {
        let response = await users.getUserById(id);
        checkStatusCode(response, 200);
        checkResponseTime(response, 300);
        userDataFromId = response.body;
    });

    it(`Compare user details by Login and by Id`, async () => {
        expect(userDataLogin.user.id).to.equal(userDataFromId.id);
        expect(userDataLogin.user.avatar).to.equal(userDataFromId.avatar);
        expect(userDataLogin.user.email).to.equal(userDataFromId.email);
        expect(userDataLogin.user.userName).to.equal(userDataFromId.userName);       
    });
});
