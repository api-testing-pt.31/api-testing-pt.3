import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new GetUserController();
const auth = new AuthController();

describe("Log in", () => {
    let accessToken: string;
    it(`Login and get the token`, async () => {
        let response = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        accessToken = response.body.token.accessToken.token;
        console.log(accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });
});
