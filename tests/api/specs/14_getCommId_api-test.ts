import { expect } from "chai";
import { PostController } from "../lib/controllers/post.controller";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";

const posts = new PostController();
const auth = new AuthController();

describe("Add comment", () => {
    let accessToken: string;
    let id;
    let postId;
    let postWithUserId;

    before(async () => {
        // This code will be executed before any test cases are run
        let responseUser = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(responseUser, 200);
        accessToken = responseUser.body.token.accessToken.token;
        id = responseUser.body.user.id;

        let response = await posts.getAllPosts();
        const responseData = response.body; // Extract the JSON data
        // Filter posts with the same id as the logged-in user
        postWithUserId = responseData.filter((post: any) => post.author.id === id);
        // Check if there is at least one post for the user
        if (postWithUserId.length > 0) {
            postId = postWithUserId[0].id; // Access the 'id' property of the first post
        } else {
            throw new Error("User does not have any posts.");
        }
    });

    it('Check comment', async () => {
        // Assuming each post has an array of comments, access the first comment of the first post
        if (postWithUserId[0].comments && postWithUserId[0].comments.length > 0) {
            const firstComment = postWithUserId[0].comments[0];
            const commentBody = firstComment.body;
            expect(commentBody).to.equal("Your creativity never fails to impress me!.");
        } else {
            throw new Error("The first post does not have any comments.");
        }
    });
});