import { expect } from "chai";
import { PostController } from "../lib/controllers/post.controller";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";

const posts = new PostController();
const auth = new AuthController();

describe("Check if the user has posts", () => {
    let accessToken: string;
    let id;

    before(async () => {
        // This code will be executed before any test cases are run
        let responseUser = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(responseUser, 200);
        accessToken = responseUser.body.token.accessToken.token;
        id = responseUser.body.user.id;
    });

    it('should check for users posts', async () => {
        let response = await posts.getAllPosts()
        const responseData = response.body; // Extract the JSON data

        // Filter posts with the same id as the logged-in user
        const postWithUserId = responseData.filter((post: any) => post.author.id === id);
        const numberOfPosts = postWithUserId.length;
        console.log("Number of user's posts: " + numberOfPosts);

    if (numberOfPosts > 0) {
        expect(numberOfPosts).to.be.at.least(1);
    } else {
        expect(numberOfPosts).to.be.at.least(0);
    }
    });
});