import { expect } from "chai";
import { PostController } from "../lib/controllers/post.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostController();

describe("Get All posts", () => {
    it('Get All posts', async () => {
        let response = await posts.getAllPosts()
        console.log(response.body);
        checkStatusCode(response, 200);
        checkResponseTime(response, 500);
    });
});