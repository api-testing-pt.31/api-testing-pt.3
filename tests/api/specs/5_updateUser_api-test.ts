import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new GetUserController();
const auth = new AuthController();


describe("Update user", () => {
    let accessToken: string;
    let userDataBeforeUpdate, userDataToUpdate;

    before(async () => {
        // This code will be executed before any test cases are run
        let response = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(response, 200);
        accessToken = response.body.token.accessToken.token;

        // Fetch current user data before update
        response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        userDataBeforeUpdate = response.body;
    });

    it(`should update username using valid data`, async () => {
        // replace the last 3 characters of actual username with random characters.
        // Another data should be without changes
        function replaceLastThreeWithRandom(str: string): string {
            return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
        }

        userDataToUpdate = {
            id: userDataBeforeUpdate.id,
            avatar: userDataBeforeUpdate.avatar,
            email: userDataBeforeUpdate.email,
            userName: replaceLastThreeWithRandom(userDataBeforeUpdate.userName),
        };

        let response = await users.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(response, 204);
    });

    it(`should return correct user details by id after updating`, async () => {
        let response = await users.getUserById(userDataBeforeUpdate.id);
        checkStatusCode(response, 200);
        expect(response.body).to.be.deep.equal(userDataToUpdate, "User details isn't correct");
    });
});