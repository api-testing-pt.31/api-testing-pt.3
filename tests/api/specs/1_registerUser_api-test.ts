import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new GetUserController();
const auth = new AuthController();

describe("Token usage", () => {
    let accessToken: string;
    
    it(`Usage is here`, async () => {
        let response = await auth.authorize(0, "myAvatar", "myNewEmail@gmail.com", "Miamarim", "myNewPassword");
        console.log(response.body);
        accessToken = response.body.token.accessToken.token;
       expect(response.statusCode, `Status Code should be 201`).to.be.equal(201);
       checkResponseTime(response, 500);
    });
});
