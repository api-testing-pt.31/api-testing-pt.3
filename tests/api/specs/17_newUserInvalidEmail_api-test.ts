import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new GetUserController();
const auth = new AuthController();

describe("Token usage", () => {
    let accessToken: string;
    
    it(`Usage is here`, async () => {
        let response = await auth.authorize(0, "myAvatar", "myNewEmail", "Miamarim", "myNewPassword");
       expect(response.statusCode, `Status Code should be 400`).to.be.equal(400);
    });
});
