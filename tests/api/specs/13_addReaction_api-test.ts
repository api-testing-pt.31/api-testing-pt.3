import { expect } from "chai";
import { PostController } from "../lib/controllers/post.controller";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";

const posts = new PostController();
const auth = new AuthController();

describe("Add comment", () => {
    let accessToken: string;
    let id;
    let postId;

    before(async () => {
        // This code will be executed before any test cases are run
        let responseUser = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(responseUser, 200);
        accessToken = responseUser.body.token.accessToken.token;
        id = responseUser.body.user.id;

        let response = await posts.getAllPosts()
        const responseData = response.body; // Extract the JSON data
        // Filter posts with the same id as the logged-in user
        const postWithUserId = responseData.filter((post: any) => post.author.id === id);
        // Check if there is at least one post for the user
        if (postWithUserId.length > 0) {
            postId = postWithUserId[0].id; // Access the 'id' property of the first post
        } else {
            throw new Error("User does not have any posts.");
        }

    });

    it('Add reaction', async () => {
        let response = await posts.addReaction(postId, true, postId, accessToken)
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });
});