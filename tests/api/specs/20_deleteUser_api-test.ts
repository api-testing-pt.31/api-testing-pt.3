import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new GetUserController();
const auth = new AuthController();

describe("Get user from Id", () => {
    let id: number;
    let accessToken: string;

    before(`Login and get the Id`, async () => {
        let response = await auth.login("myNewEmail@gmail.com", "myNewPassword");
        checkStatusCode(response, 200);
        id = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
    });

    it(`Delete user`, async () => {
        let response = await users.deleteUser(id, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response, 300);
    });

    it(`Check if the user is deleted`, async () => {
        let response = await users.getUserById(id);
        checkStatusCode(response, 404);
        checkResponseTime(response, 300);    
    });
});
