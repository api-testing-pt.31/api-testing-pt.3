"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
describe("Some controller", () => {
    it('test 1', async () => {
        console.log("It is a test #1");
    });
    it('test 2', async () => {
        console.log("It is a test #2");
        (0, chai_1.expect)(true).to.be.equal(true);
    });
    it('test 3', async () => {
        console.log("It is a test #3");
        (0, chai_1.expect)(3).to.be.equal(3);
    });
});
